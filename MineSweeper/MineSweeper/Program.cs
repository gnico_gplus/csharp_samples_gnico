﻿//----------------------------------------------------------------------
//			MineSweeper for C# Version 0.001
//			Copyright (C) 2014 by N.Tsuda
//			           Porting by Gnico
//			License: CDDL 1.0 (http://opensource.org/licenses/CDDL-1.0)
//----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;

namespace MineSweeper {
    class Program {
        static int N_MINE = 0;  //爆弾数
        static byte BD_WD = 0;  //横のマス数
        static byte BD_HT = 0; //盾のマス数

        static bool[,] g_open = new bool[2, 2];
        static bool[,] g_mine = new bool[2, 2];
        static byte[,] g_nmine = new byte[2, 2];

        static List<Point> PointList = new List<Point>();

        static void init_board() {
            g_open  = new bool[BD_WD + 2, BD_HT + 2];
            g_mine  = new bool[BD_WD + 2, BD_HT + 2];
            g_nmine = new byte[BD_WD + 2, BD_HT + 2];

            for( int x = 0; x < BD_WD + 2; ++x ) {
                for( int y = 0; y < BD_HT + 2; ++y ) {
                    g_mine[x, y]  = false;
                    g_open[x, y]  = false;
                    g_nmine[x, y] = 0;
                }
            }
        }

        static void make_bom(int NotBomX, int NotBomY) {
            for (int i = 0; i < N_MINE; ++i) {
                int x, y;
                Random r;
                do {
                    r = new Random(Environment.TickCount + i);
                    do
                        x = r.Next(1, BD_WD + 1);
                    while (x == NotBomX);
                    do
                        y = r.Next(1, BD_HT + 1);
                    while (y == NotBomY);
                } while (g_mine[x, y]);
                g_mine[x, y] = true;
                g_nmine[x - 1, y - 1] += 1;
                g_nmine[x, y - 1] += 1;
                g_nmine[x + 1, y - 1] += 1;
                g_nmine[x - 1, y] += 1;
                g_nmine[x + 1, y] += 1;
                g_nmine[x - 1, y + 1] += 1;
                g_nmine[x, y + 1] += 1;
                g_nmine[x + 1, y + 1] += 1;
            }
        }

        static void print_board() {
            Console.Write(BD_HT < 10 ? "  " : " ");
            for( int n = 1; n <= BD_WD; n++ ) {
                Console.Write(" " + n.ToString());
            }
            Console.Write("\n");
            for( int y = 1; y <= BD_HT; ++y ) {
                Console.Write(y > 9 ? y.ToString() : y.ToString() + " ");
                for( int x = 1; x <= BD_WD; ++x ) {
                    if( !g_open[x, y] ) //開いていない
                        Console.Write("■");
                    else if( g_mine[x, y] ) //周りに地雷がある
                        Console.Write("★");
                    else if( g_nmine[x, y] == 0 ) //周りに地雷なし
                        Console.Write("・");
                    else
                        Console.Write(" " + g_nmine[x, y].ToString());
                    if (x > 9 && x != BD_WD)
                        Console.Write(" ");
                }
                Console.Write("\n");
            }
            Console.Write("\n");
        }

        static int open( int x, int y ) {
            if( x < 1 || x > BD_WD || y < 1 || y > BD_HT ) //範囲外の場合
                return -1;
            if( g_open[x, y] ) //すでに開いている場合
                return -1;
            g_open[x, y] = true;
            if( !g_mine[x, y] && g_nmine[x, y] == 0 ) {
                open(x - 1, y - 1);		//	周りも開く
                open(x, y - 1);
                open(x + 1, y - 1);
                open(x - 1, y);
                open(x + 1, y);
                open(x - 1, y + 1);
                open(x, y + 1);
                open(x + 1, y + 1);
            }
            return 0;
        }

        static bool checkSweeped() {
            for( int x = 1; x <= BD_WD; ++x ) {
                for( int y = 1; y <= BD_HT; ++y ) {
                    if( !g_open[x, y] && !g_mine[x, y] )
                        return false;
                }
            }
            return true;
        }

        static void ReadPointData() {
            Console.Write("最高記録のデータを読込中です...");
            try {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load("PointData.xml");
                XmlNodeList xnodel = xdoc.GetElementsByTagName("data");
                if( xnodel.Count > 0 )
                    foreach( XmlNode n in xnodel ) {
                        PointList.Add(new Point(byte.Parse(n.Attributes[0].Value), byte.Parse(n.Attributes[1].Value), TimeSpan.Parse(n.InnerText)));
                    }
                Console.Write("読み込み完了");
            } catch( Exception ex ) {
                Console.Write("正常に読み込めませんでした");
                Console.Write("\nエラー内容\n" + ex.Message);
            }
        }

        static void Main( string[] args ) {
            Console.SetWindowSize(48, 40);
            ReadPointData();
            Console.WriteLine("\nマインスイーパ(C#版) Version 0.001");
            Console.WriteLine("開発 津田氏 Gnico\n");
            Stopwatch sw = new Stopwatch();
            while( true ) {
                sw.Reset();
                Console.WriteLine(">横の長さを設定します。数字を入力してください[2 - 29]");
                while( true ) {
                    if( byte.TryParse(Console.ReadLine(), out BD_WD) )
                        if( BD_WD > 1 && BD_HT < 30)
                            break;
                    Console.WriteLine(">>エラーです。数字を入力してください[2 - 29]");
                }
                Console.WriteLine("\n>縦の長さを設定します。数字を入力してください[2 - 254]");
                while( true ) {
                    if( byte.TryParse(Console.ReadLine(), out BD_HT) )
                        if( BD_HT > 1 && BD_HT < 255)
                            break;
                    Console.WriteLine(">>エラーです。数字を入力してください[2 - 254]");
                }
                Console.WriteLine("\n>爆弾の数を設定します。数字を入力してください[1 - " + (BD_HT * BD_WD - 1).ToString() + "]");
                while( true ) {
                    if( int.TryParse(Console.ReadLine(), out N_MINE) )
                        if( N_MINE > 0 && N_MINE < BD_HT * BD_WD - 1)
                            break;
                    Console.WriteLine(">>エラーです。数字を入力してください[1 - " + (BD_HT * BD_WD - 1).ToString() + "]\n");
                }
                init_board();
                bool sweeped = false;

                /* Turn 1 */
                sw.Start();
                print_board();
                int x, y;
                Console.WriteLine("\n>どこを開けますか ? ");
                sw.Start();
                Console.WriteLine(">開くX軸を入力してください。数字を入力してください[1 - " + BD_WD.ToString() + "]");
                while (true) {
                    if (int.TryParse(Console.ReadLine(), out x))
                        if (x > 0 && x <= BD_WD)
                            break;
                    Console.WriteLine(">>エラーです。数字を入力してください[1 - " + BD_WD.ToString() + "]");
                }
                Console.WriteLine("\n>開くY軸を入力してください。数字を入力してください[1 - " + BD_HT.ToString() + "]");
                while (true) {
                    if (int.TryParse(Console.ReadLine(), out y))
                        if (y > 0 && y <= BD_HT)
                            break;
                    Console.WriteLine(">>エラーです。数字を入力してください[1 - " + BD_HT.ToString() + "]");
                }
                make_bom(x, y);
                open(x, y);
                if (g_mine[x, y]) //爆弾にあたってしまった場合
                    break;
                sweeped = checkSweeped();
                /* End */

                while( !sweeped ) {
                    print_board();
                    Console.WriteLine("\n>どこを開けますか ? ");
                    Console.WriteLine(">開くX軸を入力してください。数字を入力してください[1 - " + BD_WD.ToString() + "]");
                    while( true ) {
                        if( int.TryParse(Console.ReadLine(), out x) )
                            if( x > 0 && x <= BD_WD )
                                break;
                        Console.WriteLine(">>エラーです。数字を入力してください[1 - " + BD_WD.ToString() + "]");
                    }
                    Console.WriteLine("\n>開くY軸を入力してください。数字を入力してください[1 - " + BD_HT.ToString() + "]");
                    while( true ) {
                        if( int.TryParse(Console.ReadLine(), out y) )
                            if( y > 0 && y <= BD_HT )
                                break;
                        Console.WriteLine(">>エラーです。数字を入力してください[1 - " + BD_HT.ToString() + "]");
                    }
                    open(x, y);
                    if( g_mine[x, y] ) //爆弾にあたってしまった場合
                        break;
                    sweeped = checkSweeped();
                }
                sw.Stop();
                print_board();
                if( sweeped ) {
                    Console.WriteLine("やったね！アナタは、爆弾以外の全てのマスを開いたよっ！");
                    bool only = true;
                    foreach( Point p in PointList ) {
                        if( p.x == BD_HT && p.y == BD_WD )
                            if (p.time > sw.Elapsed)
                                only = false;
                    }
                    if( only ) {
                        PointList.Add(new Point(BD_HT, BD_WD, sw.Elapsed));
                        Console.WriteLine("\nパチパチパチ！アナタは、最高記録を更新しました！");
                    }
                } else
                    Console.WriteLine("あちゃちゃ...爆弾にあったてしまったよ～(T_T)");
                Console.WriteLine("\n もう一回挑戦しますか ?[Y/N]");
                char s;
                while( true ) {
                    s = Convert.ToChar(Console.Read());
                    if( s == 'n' || s == 'N' ) {
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        XmlElement xroot = xdoc.CreateElement("Points");
                        xdoc.AppendChild(xroot);
                        for( int i = 0; i < PointList.Count; i++ ) {
                            XmlElement xelem = xdoc.CreateElement("data");
                            xelem.InnerText = PointList[i].time.ToString();
                            XmlAttribute xatr = xdoc.CreateAttribute("Width");
                            xelem.SetAttribute("Height", PointList[i].x.ToString());
                            xatr.Value = PointList[i].y.ToString();
                            xelem.Attributes.Append(xatr);
                            xroot.AppendChild(xelem);
                        }
                        xdoc.Save("PointData.xml");
                        Environment.Exit(0);
                    } else if( s == 'y' || s == 'Y' )
                        break;
                }
                sw.Reset();
            }
        }
    }
    class Point {
        public byte x { get; set; }
        public byte y { get; set; }
        public TimeSpan time { get; set; }
        public Point( byte a, byte b, TimeSpan c ) {
            x = a;
            y = b;
            time = c;
        }
    }
}
