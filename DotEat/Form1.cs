﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DotEat;

namespace DotEat {
    public partial class Form1 : Form {
        private Color ConsoleColor = Color.White;
        private Color BackgroundColor = Color.Black;
        private bool update = true;
        private bool GameOver = false;
        private byte count = 0;
        private int BackKey = VK_LEFT;
        public byte iv = 10;

        public Form1() {
            InitializeComponent();
        }

        public void Write( string s ) {
            ConsoleBox.SelectionLength = s.Length;
            ConsoleBox.SelectedText = s;
            ConsoleBox.SelectionColor = ConsoleColor;
            ConsoleBox.SelectionBackColor = BackgroundColor;
        }
        public void setCursorPos( int x, int y ) {
            ConsoleBox.SelectionStart = 81 * x + y;
        }
        public void setColor( Color fore, Color back ) {
            ConsoleColor = fore;
            BackgroundColor = back;
        }
        public void setColor( Color fore ) { ConsoleColor = fore; }

        int draw_map()
        {
            int nDot = 0;
            for (int y = 0; y < MAP_HT; ++y) {
                setCursorPos(0, y);
                string ptr = g_map[y];
                for( int x = 0; x < ptr.Length; ++x ) {
                    switch (ptr[x]) {
                    case '-':
                            setColor(Color.Gray, Color.Black);
                    Write("━");
                    break;
                    case '|':
                    setColor(Color.Gray, Color.Black);
                    Write("┃");
                    break;
                    case '/':
                    setColor(Color.Gray, Color.Black);
                    Write("┏");
                    break;
                    case '#':
                    setColor(Color.Gray, Color.Black);
                    Write("┓");
                    break;
                    case 'L':
                    setColor(Color.Gray, Color.Black);
                    Write("┗");
                    break;
                    case 'J':
                    setColor(Color.Gray, Color.Black);
                    Write("┛");
                    break;
                    case '.':
                    setColor(Color.Gray, Color.Black);
                    Write("・");
                    ++nDot;
                    break;
                    default:
                    setColor(Color.Gray, Color.Black);
                    Write(" ");
                    break;
                    }
                }
            }
        return nDot;
        }

        void draw_car() {
            setCursorPos(g_car.m_pos.X*2, g_car.m_pos.Y);
            setColor(Color.Green, Color.Black);
            Write("＠");
            }
        void draw_enemy() {
            setColor(Color.Red, Color.Black);
            for (int i = 0; i < g_enemy.Count; ++i) {
            setCursorPos(g_enemy[i].m_pos.X*2, g_enemy[i].m_pos.Y);
            Write("◆");
            }
        }
        void draw_score() {
            setColor(Color.Gray, Color.Black);
            setCursorPos(SCORE_X, SCORE_Y);
            Write("SCORE:");
            string str = g_score.ToString();
            while( str.Length < 6 )
                str = "0" + str;
            setCursorPos(SCORE_X, SCORE_Y + 1);
            Write(str);
        }

        public const int CONS_WD = 80;
        public const int CONS_HT = 25;
        public const int MAP_WD = (CONS_WD / 2);
        public const int MAP_HT = (CONS_HT - 1);
        public const int SCORE_X = 11 * 2;
        public const int SCORE_Y = 11;
        public const int LOOP_INTERVAL = 10;

        public const int VK_LEFT = 0x25;
        public const int VK_UP = 0x26;
        public const int VK_RIGHT = 0x27;
        public const int VK_DOWN = 0x28;

        private int g_score = 0;	// スコア
        private Car g_car;	// 自機
        private int g_level = 0;
        private int g_nEnemy = 1;	// 敵機数
        private string[] g_map = new string[MAP_HT];
        private List<Car> g_enemy = new List<Car>();	// 敵機

        public static readonly string[] mapData = new string[CONS_HT]{
            "/--------------------------------------#",
            "| . . . . . . . . . . . . . . . . . . |",
            "|./---------------- ----------------#.|",
            "| | . . . . . . . . . . . . . . . . | |",
            "|.|./-------------- --------------#.|.|",
            "| | | . . . . . . . . . . . . . . | | |",
            "|.|.|./------------ ------------#.|.|.|",
            "| | | | . . . . . . . . . . . . | | | |",
            "|.|.|.|./---------- ----------#.|.|.|.|",
            "| | | | | . . . . . . . . . . | | | | |",
            "|.|.|.|.|./------------------#.|.|.|.|.|",
	        "|         |                  |         |",
	        "|         |                  |         |",
            "|.|.|.|.|.L------------------J.|.|.|.|.|",
            "| | | | | . . . . . . . . . . | | | | |",
            "|.|.|.|.L---------- ----------J.|.|.|.|",
            "| | | | . . . . . . . . . . . . | | | |",
            "|.|.|.L------------ ------------J.|.|.|",
            "| | | . . . . . . . . . . . . . . | | |",
            "|.|.L-------------- --------------J.|.|",
            "| | . . . . . . . . . . . . . . . . | |",
            "|.L---------------- ----------------J.|",
            "| . . . . . . . . . . . . . . . . . . |",
            "L--------------------------------------J",
            "                                        ",
            };

        Vec2 rot_right90( Vec2 v ) { return new Vec2(-v.Y, v.X); }
        Vec2 rot_left90( Vec2 v ) { return new Vec2(v.Y, -v.X); }

        public void init_map() {
            for( int y = 0; y < 24; ++y )
                for( int x = 0; x < mapData[y].Length; ++x )
                    g_map[y] += mapData[y][x];
        }
        void init() {
            g_car = new Car(new Vec2(MAP_WD / 2 - 1, 1),new Vec2( -1, 0));
            add_enemy();
            init_map();
        }
        public void add_enemy() {
            g_enemy.Clear();
            g_enemy.Add(new Car(new Vec2(MAP_WD / 2 + 1, 1), new Vec2(1, 0)));
            if( g_nEnemy == 1 ) return;
            g_enemy.Add(new Car(new Vec2(MAP_WD / 2 + 1, MAP_HT - 2), new Vec2(-1, 0)));
            if( g_nEnemy == 2 ) return;
            g_enemy.Add(new Car(new Vec2(9, MAP_HT / 2 + 1), new Vec2(0, -1), /*lane:*/4));
        }
        bool can_move_to( Vec2 x ) {
            return g_map[x.Y][x.X] == ' ' || g_map[x.Y][x.X] == '.';
        }
        bool can_move_to( ref Vec2 pos ) {
            return g_map[pos.Y][pos.X] == ' ' || g_map[pos.Y][pos.X] == '.';
        }
        public void move_car( ref Car c ) {
            Vec2 p = c.m_pos + c.m_v;	// 次の位置
            if( !can_move_to(p) ) {	// 壁にぶつかった場合
                Vec2 v = rot_right90(c.m_v);	// 速度ベクターを90度回転
                p = c.m_pos + v;
                if( can_move_to(p) )
                    c.m_v = v;
                else {
                    // 行き止まりは無いと仮定
                    c.m_v = rot_left90(c.m_v);	// 速度ベクターを90度回転
                    p = c.m_pos + c.m_v;
                }
                c.m_pos = p;
            }
        }
        public void move_car_list( ref List<Car> c, int index ) {
            Vec2 p = c[index].m_pos + c[index].m_v;	// 次の位置
            if( !can_move_to(p) ) {	// 壁にぶつかった場合
                Vec2 v = rot_right90(c[index].m_v);	// 速度ベクターを90度回転
                p = c[index].m_pos + v;
                if( can_move_to(p) )
                    c[index] = new Car( c[index].m_pos,v,c[index].m_lane,c[index].m_laneChanged );
                else {
                    // 行き止まりは無いと仮定
                    c[index]  = new Car( c[index].m_pos,rot_left90(c[index].m_v),c[index].m_lane,c[index].m_laneChanged );
                    p = c[index].m_pos + c[index].m_v;
                }
                c[index] = new Car(p, c[index].m_v, c[index].m_lane, c[index].m_laneChanged);
            }
        }
        public void change_lane( ref Car car, ref int key ) {
            Vec2 p = car.m_pos;
            if( can_move_to(p) && can_move_to(p + car.m_v) ) {	// 行き止まりでない場合
                if( car.m_v.Y == 0 ) {	// 水平方向に移動している場合
                    if( key == VK_UP && can_move_to(p - new Vec2(0, 1)) ) {
                        if( (p.Y -= 2) < MAP_HT / 2 )
                            --car.m_lane;	// 外側のレーンに移動
                        else {
                            ++car.m_lane;
                        }
                        key = 0;
                    } else if( key == VK_DOWN && can_move_to(p + new Vec2(0, 1)) ) {
                        if( (p.Y += 2) > MAP_HT / 2 )
                            --car.m_lane;	// 外側のレーンに移動
                        else
                            ++car.m_lane;
                        key = 0;
                    }
                } else {	// 垂直方向に移動している場合
                    if( key == VK_LEFT && can_move_to(p - new Vec2(1, 0)) ) {
                        if( (p.X -= 2) < MAP_WD / 2 )
                            --car.m_lane;	// 外側のレーンに移動
                        else
                            ++car.m_lane;
                        key = 0;
                    } else if( key == VK_RIGHT && can_move_to(p + new Vec2(1, 0)) ) {
                        if( (p.X += 2) > MAP_WD / 2 )
                            --car.m_lane;	// 外側のレーンに移動
                        else
                            ++car.m_lane;
                        key = 0;
                    }
                }
            }
            car.m_pos = p;
        }
        public void change_lane_list( ref List<Car> car,ref int key, int index ) {
            Vec2 p = car[index].m_pos;
            if( can_move_to(p) && can_move_to(p + car[index].m_v) ) {	// 行き止まりでない場合
                if( car[index].m_v.Y == 0 ) {	// 水平方向に移動している場合
                    if( key == VK_UP && can_move_to(p - new Vec2(0, 1)) ) {
                        if( (p.Y -= 2) < MAP_HT / 2 )
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane - 1, car[index].m_laneChanged); // 外側のレーンに移動
                        else {
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane + 1, car[index].m_laneChanged);
                        }
                        key = 0;
                    } else if( key == VK_DOWN && can_move_to(p + new Vec2(0, 1)) ) {
                        if( (p.Y += 2) > MAP_HT / 2 )
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane - 1, car[index].m_laneChanged); // 外側のレーンに移動
                        else
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane + 1, car[index].m_laneChanged);
                        key = 0;
                    }
                } else {	// 垂直方向に移動している場合
                    if( key == VK_LEFT && can_move_to(p - new Vec2(1, 0)) ) {
                        if( (p.X -= 2) < MAP_WD / 2 )
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane - 1, car[index].m_laneChanged); // 外側のレーンに移動
                        else
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane + 1, car[index].m_laneChanged);
                        key = 0;
                    } else if( key == VK_RIGHT && can_move_to(p + new Vec2(1, 0)) ) {
                        if( (p.X += 2) > MAP_WD / 2 )
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane - 1, car[index].m_laneChanged); // 外側のレーンに移動
                        else
                            car[index] = new Car(car[index].m_pos, car[index].m_v, car[index].m_lane + 1, car[index].m_laneChanged);
                        key = 0;
                    }
                }
            }
            car[index] = new Car(p, car[index].m_v, car[index].m_lane, car[index].m_laneChanged); // 外側のレーンに移動
        }
        // 敵機を自機の方にレーン変更
        // 一番外側が レーン０，一番内側が レーン４
        public void change_lane( ref Car car ) {
            if( car.m_laneChanged || /* ２回連続レーンチェンジ不可 */ car.m_lane == g_car.m_lane || !can_move_to(car.m_pos + car.m_v) )	/* 行き止まりの場合 */ {
                car.m_laneChanged = false;
                return;
            }
            if( car.m_lane > g_car.m_lane ) {	// 外側のレーンに移動
                if( car.m_v.Y == 0 ) {	// 水平方向に移動している場合
                    if( car.m_pos.Y < MAP_HT / 2 ) {
                        if( can_move_to(car.m_pos - new Vec2(0, 1)) ) {
                            car.m_pos.Y -= 2;
                            --car.m_lane;
                            car.m_laneChanged = true;
                        }
                    } else {
                        if( g_level == 0 ) return;
                        if( can_move_to(car.m_pos + new Vec2(0, 1)) ) {
                            car.m_pos.Y += 2;
                            --car.m_lane;
                            car.m_laneChanged = true;
                        }
                    }
                } else {
                }
            } else {	// 内側のレーンに移動
                if( car.m_v.Y == 0 ) {	// 水平方向に移動している場合
                    if( car.m_pos.Y < MAP_HT / 2 ) {
                        if( can_move_to(car.m_pos + new Vec2(0, 1)) ) {
                            car.m_pos.Y += 2;
                            ++car.m_lane;
                            car.m_laneChanged = true;
                        }
                    } else {
                        if( g_level == 0 ) return;
                        if( can_move_to(car.m_pos - new Vec2(0, 1)) ) {
                            car.m_pos.Y -= 2;
                            ++car.m_lane;
                            car.m_laneChanged = true;
                        }
                    }
                } else {
                }
            }
            if( !(car.m_lane >= 0 && car.m_lane < 5) ) Environment.Exit(-1);
        }
        // 加減速処理
        public void accel_decel( ref int key, ref int iv ) {
            if( g_car.m_v.X > 0 ) {
                if( key == VK_RIGHT ) {
                    iv = 5;
                    key = 0;
                } else if( key == VK_LEFT ) {
                    iv = 20;
                    key = 0;
                }
            } else if( g_car.m_v.X < 0 ) {
                if( key == VK_RIGHT ) {
                    iv = 20;
                    key = 0;
                } else if( key == VK_LEFT ) {
                    iv = 5;
                    key = 0;
                }
            } else if( g_car.m_v.Y > 0 ) {
                if( key == VK_DOWN ) {
                    iv = 5;
                    key = 0;
                } else if( key == VK_UP ) {
                    iv = 20;
                    key = 0;
                }
            } else if( g_car.m_v.Y < 0 ) {
                if( key == VK_DOWN ) {
                    iv = 20;
                    key = 0;
                } else if( key == VK_UP ) {
                    iv = 5;
                    key = 0;
                }
            }
        }
        public struct Car {
            public Vec2 m_pos;
            public Vec2 m_v;
            public int m_lane;
            public bool m_laneChanged;
            public Car( Vec2 pos, Vec2 v, int lane = 0,bool change = false ) {
                m_pos = pos;
                m_v = v;
                m_lane = lane;
                m_laneChanged = change;
            }
        }
        public class Vec2 {
            public int X { get; set; }
            public int Y { get; set; }
            public Vec2( int x, int y ) {
                X = x;
                Y = y;
            }
            public static Vec2 operator +( Vec2 x, Vec2 y ) { return new Vec2(x.X + y.X, y.X + y.Y); }
            public static Vec2 operator -( Vec2 x, Vec2 y ) { return new Vec2(x.X - y.X, y.X - y.Y); }
        }
        void eat_dot() {
            if( g_map[g_car.m_pos.Y][g_car.m_pos.X] == '.' ) {
                g_map[g_car.m_pos.Y] = g_map[g_car.m_pos.Y].Substring(0, g_car.m_pos.X) + " " + g_map[g_car.m_pos.Y].Substring(g_car.m_pos.X + 1);
                g_score += 10;
                //mciSendString(TEXT("play button57.mp3"), NULL, 0, NULL);
            }
        }
        bool check_crash()
        {
            for (int i = 0; i < g_enemy.Count; ++i) {
                Car c = g_enemy[i];
                if( c.m_pos == g_car.m_pos )
                    return true;
                if( c.m_pos == g_car.m_pos - g_car.m_v )
                    return true;	// すり抜けた場合
            }
            return false;
        }
        private void BaseTimerTick( object sender, EventArgs e ) {
            if( GameOver && MessageBox.Show("Try Again?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.Cancel ) {
                Environment.Exit(0);
            } else {
                GameOver = false;
            }
            init();
            if( update ) {
                int nDot = draw_map();
                draw_enemy();
                draw_car();
                draw_score();
                if( nDot == 0 ) {	// ドット全消去
                    //mciSendString(TEXT("play one23.mp3"), NULL, 0, NULL);
                    g_score += g_nEnemy * 1000;
                    if( ++g_level > 1 )
                        ++g_nEnemy;
                    init();
                }
                if( check_crash() ) {
                    //mciSendString(TEXT("play s-burst01.mp3"), NULL, 0, NULL);
                    GameOver = true;
                }
            }
            update = false;
            if( count % 10 == 0 ) {
                for( int i = 0; i < g_enemy.Count; ++i ) {
                    move_car_list(ref g_enemy, i);
                    change_lane_list(ref g_enemy,ref BackKey, i);
                }
                update = true;
            }
            if( count % iv == 0 ) {
                move_car(ref g_car);
                change_lane(ref g_car,ref BackKey);
                //eat_dot();
                update = true;
                iv = 10;
            }
            if( count == 10 )
                count = 0;
        }

        private void Window_keyUp( object sender, KeyEventArgs e ) {
            switch( e.KeyData ) {
                case Keys.Left:
                    BackKey = VK_LEFT;
                    break;
                case Keys.Right:
                    BackKey = VK_RIGHT;
                    break;
                case Keys.Up:
                    BackKey = VK_UP;
                    break;
                case Keys.Down:
                    BackKey = VK_DOWN;
                    break;
            }
        }
    }
}
