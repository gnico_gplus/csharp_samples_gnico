﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Threading;

namespace BatteryBar
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        PowerStatus ps = SystemInformation.PowerStatus;
        DispatcherTimer tp = new DispatcherTimer();

        public MainWindow() {
            tp.Interval = new TimeSpan(0,0,0,10);
            InitializeComponent();
            update();
            tp.Tick += ((sender, e) => update());
            tp.Start();
        }

        private void update() {
            switch( ps.BatteryChargeStatus ) {
                case BatteryChargeStatus.High:
                    Progress.Foreground = new SolidColorBrush(Colors.LimeGreen);
                    break;
                case BatteryChargeStatus.Low:
                    Progress.Foreground = new SolidColorBrush(Colors.Yellow);
                    break;
                case BatteryChargeStatus.Critical:
                    Progress.Foreground = new SolidColorBrush(Colors.Red);
                    break;
            }
            switch( ps.PowerLineStatus ) {
                case System.Windows.Forms.PowerLineStatus.Online:
                    Status.Text = "AC電源";
                    LifeRemaining.Text = "";
                    Progress.Value = ps.BatteryLifePercent * 100;
                    Progress.Foreground = new SolidColorBrush(Colors.DodgerBlue);
                    break;
                case System.Windows.Forms.PowerLineStatus.Offline:
                    Status.Text = "バッテリ";
                    TimeSpan ts = new TimeSpan(0, 0, ps.BatteryLifeRemaining);
                    LifeRemaining.Text = String.Format("残り時間\n" + ts.ToString());
                    Progress.Value = ps.BatteryLifePercent * 100;
                    break;
                case System.Windows.Forms.PowerLineStatus.Unknown:
                    Status.Text = "不明";
                    Progress.Foreground = new SolidColorBrush(Colors.Yellow);
                    break;
            }
        }

        private void Move_Windows( object sender, MouseButtonEventArgs e ) {
            DragMove();
        }

        private void MenuItem_Click( object sender, RoutedEventArgs e ) {
            Environment.Exit(0);
        }

    }
}
