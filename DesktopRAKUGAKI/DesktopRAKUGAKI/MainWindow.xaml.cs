﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

namespace DesktopRAKUGAKI {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }
        bool HideI = false;
        bool backI = false;
        private void Window_keyUp(object sender, KeyEventArgs e) {
            switch(e.Key){
                case Key.Escape: Environment.Exit(0); break;
                case Key.C:
                    System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();
                    cd.AnyColor = true;
                    if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                        ic.DefaultDrawingAttributes.Color = Color.FromArgb(cd.Color.A,  cd.Color.R, cd.Color.G, cd.Color.B);
                    }
                    break;
                case Key.Back: ic.Strokes.Clear(); break;
                case Key.B: 
                    ic.Background = backI ? new SolidColorBrush(Color.FromArgb(1,255,255,255)) : new SolidColorBrush(Color.FromArgb(75,255,255,255));
                    backI = !backI;
                    break;
                case Key.H:
                    if (HideI) {
                        ic.Background = new SolidColorBrush(Color.FromArgb(1, 255, 255, 255));
                        FocusBtn.Visibility = Visibility.Collapsed;
                    } else {
                        ic.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
                        FocusBtn.Visibility = Visibility.Visible;
                    }
                    HideI = !HideI;
                    break;
                case Key.D: ic.EditingMode = InkCanvasEditingMode.EraseByPoint; break;
                case Key.I: ic.EditingMode = InkCanvasEditingMode.Ink; break;
                case Key.X: ic.EditingMode = InkCanvasEditingMode.EraseByStroke; break;
                case Key.O:
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "InkSerializedFormatファイル|*.isf";
                    bool? resluto = ofd.ShowDialog();
                    if (resluto == true) {
                        ic.Strokes.Clear();
                        using (System.IO.FileStream fs = new System.IO.FileStream(ofd.FileName,System.IO.FileMode.Open)){
                            ic.Strokes = new System.Windows.Ink.StrokeCollection(fs);
                        }
                    }
                    break;
                case Key.S:
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FilterIndex = 1;
                    sfd.AddExtension = true;
                    bool? reslut;
                    if (MessageBox.Show("デスクトップのキャプチャもしますか？", "保存範囲", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                        sfd.Filter = "JEPG画像ファイル|*.jpg";
                        reslut = sfd.ShowDialog();
                        if (reslut == true) {
                            BitmapSource BI = CopyScreen();
                            using (System.IO.FileStream fs = new System.IO.FileStream(sfd.FileName, System.IO.FileMode.Create)) {
                                JpegBitmapEncoder enc = new JpegBitmapEncoder();
                                enc.Frames.Add(BitmapFrame.Create(BI));
                                enc.Save(fs);
                            }
                        }
                    } else {
                        if (MessageBox.Show("他のプログラムでも、読み込み可能な形式で保存しますか？\n(このプログラムでは、再度読み込めません)", "保存範囲", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                            sfd.Filter = "JPEG画像ファイル|*.jpg|PNG画像ファイル|*.png";
                            reslut = sfd.ShowDialog();
                            if (reslut == true) {
                                string Extention = System.IO.Path.GetExtension(sfd.FileName).ToUpper();
                                ic.Background = new SolidColorBrush(Color.FromArgb(0,255,255,255));
                                Rect rectBounds = ic.Strokes.GetBounds();
                                DrawingVisual DV = new DrawingVisual();
                                DrawingContext DC = DV.RenderOpen();
                                DC.PushTransform(new TranslateTransform(-rectBounds.X,-rectBounds.Y));
                                if (Extention == ".PNG") {
                                    DC.DrawRectangle(ic.Background, null, rectBounds);
                                } else {
                                    System.Windows.Forms.ColorDialog cdi = new System.Windows.Forms.ColorDialog();
                                    cdi.AnyColor = true;
                                    MessageBox.Show("背景色を選択してください");
                                    if (cdi.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                                        DC.DrawRectangle(new SolidColorBrush(Color.FromRgb(cdi.Color.R, cdi.Color.G, cdi.Color.B)), null, rectBounds);
                                    } else {
                                        MessageBox.Show("デフォルトの白を指定します。");
                                        DC.DrawRectangle(new SolidColorBrush(Color.FromRgb(0,0,0)), null, rectBounds);
                                    }
                                }
                                ic.Strokes.Draw(DC);
                                DC.Close();
                                RenderTargetBitmap RTB = new RenderTargetBitmap((int)rectBounds.Width,(int)rectBounds.Height,96,96,PixelFormats.Default);
                                RTB.Render(DV);
                                BitmapEncoder enc = null;
                                switch (Extention) {
                                    case ".PNG": enc = new PngBitmapEncoder(); break;
                                    case ".JPG": enc = new JpegBitmapEncoder(); break;
                                }
                                if (enc != null) {
                                    using (System.IO.FileStream sf = new System.IO.FileStream(sfd.FileName, System.IO.FileMode.Create)) {
                                        enc.Frames.Add(BitmapFrame.Create(RTB));
                                        enc.Save(sf);
                                    }
                                }
                                ic.Background = new SolidColorBrush(Color.FromArgb(1, 255, 255, 255));
                            }
                        } else {
                            sfd.Filter = "InkSerializedFormatファイル|*.isf";
                            reslut = sfd.ShowDialog();
                            if (reslut == true){
                                using (System.IO.FileStream fs = new System.IO.FileStream(sfd.FileName, System.IO.FileMode.Create)) {
                                    ic.Strokes.Save(fs);
                                }
                            }
                        }
                    }
                    break;
                case Key.PageUp:
                    ic.DefaultDrawingAttributes.Height += 2;
                    ic.DefaultDrawingAttributes.Width += 2;
                    break;
                case Key.PageDown:
                    ic.DefaultDrawingAttributes.Height -= ic.DefaultDrawingAttributes.Height > 3 ? 2 : 0;
                    ic.DefaultDrawingAttributes.Width -= ic.DefaultDrawingAttributes.Width > 3 ? 2 : 0;
                    break;
                case Key.Z:
                    ic.UndoStroke();
                    break;
            }
        }
        private static BitmapSource CopyScreen() {
            using (var screenBmp = new System.Drawing.Bitmap(
                (int)SystemParameters.PrimaryScreenWidth,
                (int)SystemParameters.PrimaryScreenHeight,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb)) {
                    using (var bmpGraphics = System.Drawing.Graphics.FromImage(screenBmp)) {
                    bmpGraphics.CopyFromScreen(0, 0, 0, 0, screenBmp.Size);
                    return Imaging.CreateBitmapSourceFromHBitmap(
                        screenBmp.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                }
            }
        }

        private void FocusBtn_Click(object sender, RoutedEventArgs e) {
            if (HideI) {
                ic.Background = new SolidColorBrush(Color.FromArgb(1, 255, 255, 255));
                FocusBtn.Visibility = Visibility.Collapsed;
            } else {
                ic.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
                FocusBtn.Visibility = Visibility.Visible;
            }
            HideI = !HideI;
        }
    }
    public class SuperInkCanvas : InkCanvas {
        /// <summary>
        /// アンドゥバッファ
        /// </summary>
        private List<System.Windows.Ink.StrokeCollection> UndoBufferList = new List<System.Windows.Ink.StrokeCollection>();
        /// <summary>
        /// 直前が描画だったか、消しゴムだったかの列挙体
        /// </summary>
        private enum LAST_STROKE_TYPE {
            STROKE,
            ERASER
        };
        /// <summary>
        /// 直前が描画だったか、消しゴムだったかの判断をするためのフラグ
        /// </summary>
        private LAST_STROKE_TYPE LastStrokeType = LAST_STROKE_TYPE.STROKE;
        /// <summary>
        /// 静的コンストラクタ
        /// </summary>
        static SuperInkCanvas() {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SuperInkCanvas), new FrameworkPropertyMetadata(typeof(SuperInkCanvas)));
        }
        /// <summary>
        /// 通常のコンストラクタ
        /// </summary>
        public SuperInkCanvas() {
            this.StrokeCollected += new InkCanvasStrokeCollectedEventHandler(UndoInkCanvas_StrokeCollected);
            this.StrokeErased += new RoutedEventHandler(UndoInkCanvas_StrokeErased);
        }
        /// <summary>
        /// キャンバスのストロークを全消去します。
        /// </summary>
        public void ClearCanvas() {
            this.UndoBufferList.Clear();
            this.Strokes.Clear();
        }
        /// <summary>
        /// ストロークの取り消し
        /// </summary>
        public void UndoStroke() {
            if (this.UndoBufferList.Count > 1) {
                //直前の描画結果を破棄
                this.UndoBufferList.RemoveAt(this.UndoBufferList.Count - 1);
                //直前の描画前の状態を復活
                this.Strokes.Clear();
                foreach (var stroke in this.UndoBufferList[UndoBufferList.Count - 1]) {
                    this.Strokes.Add(stroke);
                }
            } else {
                //Undoリストが無いので、全部消去
                this.UndoBufferList.Clear();
                this.Strokes.Clear();
            }
        }
        /// <summary>
        /// ストロークが追加されたイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UndoInkCanvas_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e) {
            this.UndoBufferList.Add(this.Strokes.Clone());
            LastStrokeType = LAST_STROKE_TYPE.STROKE;
        }
        /// <summary>
        /// 消しゴムが実行されたイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UndoInkCanvas_StrokeErased(object sender, RoutedEventArgs e) {
            if (LastStrokeType == LAST_STROKE_TYPE.STROKE) {
                //直前が描画ならば、履歴の追加
                this.UndoBufferList.Add(this.Strokes.Clone());
            } else {
                //削除イベントが連続しているので、直前の描画結果を更新
                if (this.UndoBufferList.Count > 0) {
                    this.UndoBufferList[this.UndoBufferList.Count - 1] = this.Strokes.Clone();
                }
            }
            LastStrokeType = LAST_STROKE_TYPE.ERASER;
        }
    }
}