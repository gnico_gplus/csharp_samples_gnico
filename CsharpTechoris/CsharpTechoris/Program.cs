﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CsharpTechoris {
    class Program {
        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(int vKey);
        static bool isKeyPressed( int vKey ) { return (GetAsyncKeyState(vKey) & 0x8000) != 0; }


        static int VK_LEFT = 0x25;
        static int VK_UP = 0x26;
        static int VK_RIGHT = 0x27;
        static int VK_DOWN = 0x28;

        static int CONS_WD = 80;
        static int CONS_HT = 25;
        static int BD_WIDTH = 10;
        static int BD_HEIGHT = 20;
        static byte N_TETRIS = 7;
        static byte WALL = 0xff;
        const int TR_WIDTH = 4;
        const int TR_HEIGHT = 4;
        static int BD_ORG_X = 2;
        static int BD_ORG_Y = 1;
        static int SCORE_X = BD_ORG_X + (BD_WIDTH + 2) * 2 + 4;
        static int SCORE_Y = BD_ORG_Y;

        static int FALL_INTERVAL = 30;
        static int MOVE_INTERVAL = 15;
        static int ROT_INTERVAL = 15;

        static Random rand;

        static int	g_type;		    //	テトリスタイプ
        static int	g_x, g_y;	   	//	落下テトリス位置（盤面左上からの相対位置）
        static int	g_rotIX;			//	回転番号
        static int g_score;
        static byte[,] g_tetris = new byte[TR_WIDTH, TR_HEIGHT];			//	落下テトリス
        static byte[,] g_board = new byte[BD_WIDTH+2, BD_HEIGHT+2]; 	//	外枠を含めた盤面
        //落下テトリミノ設定
        static byte[, , ,] trData = new byte[7, 4, TR_HEIGHT, TR_WIDTH]{
	{	//	I
		{
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
		},
		{
			{0, 0, 0, 0},
			{1, 1, 1, 1},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
		},
		{
			{0, 0, 0, 0},
			{1, 1, 1, 1},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	o
		{
			{1, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	Z
		{
			{1, 1, 0, 0},
			{0, 1, 1, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 0, 0},
			{1, 1, 0, 0},
			{1, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 1, 0, 0},
			{0, 1, 1, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 0, 0},
			{1, 1, 0, 0},
			{1, 0, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	S
		{
			{0, 1, 1, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 0, 0, 0},
			{1, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 1, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 0, 0, 0},
			{1, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	L
		{
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 1, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 0, 1, 0},
			{1, 1, 1, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 0, 0, 0},
			{1, 1, 1, 0},
			{1, 0, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	J
		{
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 0, 0, 0},
			{1, 1, 1, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 1, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{1, 0, 0, 0},
			{1, 1, 1, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
	},
	{	//	T
		{
			{0, 1, 0, 0},
			{1, 1, 1, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 0, 0},
			{1, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 0, 0, 0},
			{1, 1, 1, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
		{
			{0, 1, 0, 0},
			{0, 1, 1, 0},
			{0, 1, 0, 0},
			{0, 0, 0, 0},
		},
	},
};
        static void init_board() {
            for( int x = 1; x < BD_WIDTH + 1; ++x )
            for( int y = 1; y < BD_HEIGHT + 1; ++y )
                g_board[x, y] = 0; // EMPTY
            for( int x = 0; x < BD_WIDTH + 2; ++x )
                g_board[x, 0] = g_board[x, BD_HEIGHT + 1] = WALL;
            for( int y = 0; y < BD_HEIGHT + 2; ++y )
                g_board[0, y] = g_board[BD_WIDTH + 1, y] = WALL;
        }

        static void print_frame() { //外枠描画
            Console.BackgroundColor = Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(BD_ORG_X, BD_ORG_Y);
            for( int x = 0; x < BD_WIDTH + 2; ++x )
                Console.Write("  ");
            Console.SetCursorPosition(BD_ORG_X, BD_ORG_Y + BD_HEIGHT + 1);
            for( int x = 0; x < BD_WIDTH + 2; ++x )
                Console.Write("  ");
            for( int y = BD_ORG_Y + 1; y < BD_ORG_Y + BD_HEIGHT + 1; ++y ) {
                Console.SetCursorPosition(BD_ORG_X, y);
                Console.Write("  ");
                Console.SetCursorPosition(BD_ORG_X + (BD_WIDTH + 1) * 2, y);
                Console.Write("  ");
            }
        }

        static void print_board() {
            for(int y = 1; y <= BD_HEIGHT; ++y){
                Console.SetCursorPosition(BD_ORG_X + 2, y + BD_ORG_Y);
                for(int x = 1; x <= BD_WIDTH; ++x){
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.BackgroundColor = g_board[x, y] != 0 ? ConsoleColor.Green : ConsoleColor.Black;
                    Console.Write("  ");
                }
            }
        }

        static void print_score() {
            Console.SetCursorPosition(SCORE_X, SCORE_Y);
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("SCORE:");
            Console.Write(g_score.ToString());
        }

        static void setTetris( int type, int rx ) {
            for( int y = 0; y < TR_HEIGHT; ++y )
                for( int x = 0; x < TR_WIDTH; ++x )
                    g_tetris[x, y] = trData[type, rx, y, x];
        }

        static void setTetris() {
            g_x = (BD_WIDTH - TR_WIDTH) / 2;
            g_y = 0;
            rand = new Random(Environment.TickCount);
            g_type = rand.Next(N_TETRIS);
            setTetris(g_type, g_rotIX = 0);
        }

        static void draw_tetris() {
            Console.BackgroundColor = Console.ForegroundColor = ConsoleColor.Blue;
            for( int i = 0; i < TR_WIDTH; ++i ) {
                int y = g_y + i;
                if( y < 0 || y >= BD_HEIGHT )
                    continue;
                for( int k = 0; k < TR_WIDTH; ++k ) {
                    int x = g_x + k;
                    if( x < 0 || x >= BD_WIDTH )
                        continue;
                    if( g_tetris[k, i] != 0 ) {
                        Console.SetCursorPosition(BD_ORG_X + (x + 1) * 2, BD_ORG_Y + y + 1);
                        Console.Write("  ");
                    }
                }
            }
        }

        //	移動可能チェック
        static bool can_move_left() {
            for( int y = 0; y < TR_HEIGHT; ++y ) 
                for( int x = 0; x < TR_WIDTH; ++x ) 
                    if( g_tetris[x,y] != 0 ) {		//	ブロックがある
                        if( g_board[x + g_x + 1 - 1,y + g_y + 1] != 0 )
                            return false;				//	すぐ左に壁 or 固定ブロックがある
                        break;		//	この行は左に移動可能
                    }
            return true;
        }
        static bool can_move_right() {
            for( int y = 0; y < TR_HEIGHT; ++y ) 
                for( int x = TR_WIDTH; --x >= 0; )
                    if( g_tetris[x, y] != 0 ) {		//	ブロックがある
                        if( g_board[x + g_x + 1 + 1, y + g_y + 1] % 2 == 1 )
                            return false;				//	すぐ右に壁 or 固定ブロックがある
                        break;		//	この行は右に移動可能
                    }
            return true;
        }
        static bool can_move_down() {
            for( int x = 0; x < TR_WIDTH; ++x ) 
                for( int y = TR_HEIGHT; --y >= 0; ) 
                    if( g_tetris[x,y] != 0 ) {		//	ブロックがある
                        if( g_board[x + g_x + 1,y + g_y + 1 + 1] != 0 )
                            return false;				//	すぐ下に壁 or 固定ブロックがある
                        break;		//	この列は下に移動可能
                    }
            return true;
        }

        //	落下テトリスを固定ブロックに置き換える
        static void to_fixed() {
            for( int y = 0; y < TR_HEIGHT; ++y ) {
                for( int x = 0; x < TR_WIDTH; ++x ) {
                    if( g_tetris[x,y] != 0 )
                        g_board[x + g_x + 1,y + g_y + 1] = 1;
                }
            }
        }
        static void move_down( int y ) {
            while( y > 1 ) {
                for( int x = 1; x <= BD_WIDTH; ++x )
                    g_board[x,y] = g_board[x,y - 1];		//	１行下に移動
                --y;		//	上の行に
            }
            for( int x = 1; x <= BD_WIDTH; ++x )
                g_board[x,1] = 0;		//	最上行は空に
        }
        //	揃ったラインを消去
        //	揃う可能性があるのは、落下テトリス位置だけなので、そこのみチェック
        static void check_line_filled() {
            int nClear = 0;		//	消去したライン数
            for( int ty = 0; ty < TR_HEIGHT; ++ty ) {
                int y = ty + g_y + 1;
                if( y > BD_HEIGHT ) break;
                int cnt = 0;
                for( int x = 1; x <= BD_WIDTH; ++x ) 
                    if( g_board[x,y] != 0 )
                        ++cnt;
                if( cnt == BD_WIDTH ) {	//	１行揃っている場合
                    move_down(y);				//	y 行を消し、固定ブロックを１行下に移動
                    ++nClear;
                }
            }
            switch( nClear ) {
                case 1: g_score += 10; break;
                case 2: g_score += 40; break;
                case 3: g_score += 90; break;
                case 4: g_score += 160; break;
            }
        }

        //	落下テトリスが固定ブロックと重なっているか？
        static bool is_overlaped()
        {
	        for (int y = 0; y < TR_HEIGHT; ++y) {
		        for (int x = 0; x < TR_WIDTH; ++x) {
			        if( g_tetris[x,y] != 0 && g_board[x+g_x+1,y+g_y+1] != 0 )
				        return true;
		        }
	        }
	        return false;
        }

    static void game()
    {
	    g_score = 0;
	    init_board();
	    print_frame();
	    print_board();
	    print_score();
	    setTetris();
	    draw_tetris();
	    int key = 0;	//	押下されたキー
	    int keyDown = 0;	//	押下状態のキー
	    for (int cnt = 1; ; ++cnt) {
		    bool update = false;		//	画面更新フラグ
		    if( cnt % FALL_INTERVAL == 0 || key == VK_DOWN )
		    {		//	落下処理
			    if( !can_move_down() ) {
				    key = 0;
				    to_fixed();
				    check_line_filled();		//	揃ったラインを消去
				    print_score();
				    setTetris();
				    print_board();
				    draw_tetris();
				    if( is_overlaped() )
					    break;
				    continue;
				    //break;
			    }
			    ++g_y;		//	落下中テトリスをひとつ下に移動
			    update = true;
		    }
		    if( cnt % MOVE_INTERVAL == 0 ) {		//	左右移動処理
			    if( key == VK_LEFT ) {
				    if( can_move_left() ) {
					    --g_x;
					    update = true;
				    }
			    } else if( key == VK_RIGHT ) {
				    if( can_move_right() ) {
					    ++g_x;
					    update = true;
				    }
			    }
                key = 0;
		    }
		    if( cnt % ROT_INTERVAL == 0 ) {		//	回転処理
			    if( key == VK_UP ) {
				    int tx = g_rotIX;
				    if( ++tx >= 4 ) tx = 0;
				    setTetris(g_type, tx);
				    if( is_overlaped() ) {		//	回転出来ない場合
					    setTetris(g_type, g_rotIX);		//	落下テトリスを元に戻す
				    } else {
					    g_rotIX = tx;
					    update = true;
				    }
				    key = 0;
			    }
		    }
		    if( update ) {
			    print_board();
			    draw_tetris();
		    }
		    if( keyDown == 0) {		//	キー押下を受け付けていない場合
			    if( isKeyPressed(VK_LEFT) ) {
				    key = keyDown = VK_LEFT;
			    } else if( isKeyPressed(VK_RIGHT) ) {
				    key = keyDown = VK_RIGHT;
			    } else if( isKeyPressed(VK_UP) ) {
				    key = keyDown = VK_UP;
			    } else if( isKeyPressed(VK_DOWN) ) {
				    key = keyDown = VK_DOWN;
			    }
		    } else {
			    if( !isKeyPressed(keyDown) )	//	押されたキーが離された
				    keyDown = 0;
		    }
#if	false
		if( isKeyPressed(VK_LEFT) ) {
			--g_x;
		} else if( isKeyPressed(VK_RIGHT) ) {
			++g_x;
		}
#endif
		System.Threading.Thread.Sleep(10);		//	10ミリ秒ウェイト
	}
}


        static void Main( string[] args ) {
#if	false
	init_board();
	draw_frame();
	draw_board();
	draw_score();
	setTetris();
	draw_tetris();
	getchar();
#endif
#if	true
	for (;;) {
		game();
		Console.SetCursorPosition(0, CONS_HT - 1);
        Console.BackgroundColor = ConsoleColor.Gray;
        Console.ForegroundColor = ConsoleColor.Black;
		Console.Write("GAME OVER. Try Again ? [y/n] ");
#if	true
		for (;;) {
			if( isKeyPressed('N') )
				Environment.Exit(0);
			if( isKeyPressed('Y') )
				break;
			System.Threading.Thread.Sleep(10);		//	10ミリ秒ウェイト
		}
#else
		string buf;
		buf = Console.ReadLine();
		if( buf == "n" || buf == "N" ) Enviroment.Exit(0);
#endif
		Console.SetCursorPosition(0, CONS_HT - 1);
		for (int i = 0; i < CONS_WD - 1; ++i) {
            Console.Write(" ");
		}
	}
#endif
        }
    }
}
