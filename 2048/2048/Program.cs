﻿//----------------------------------------------------------------------
//		2048 for C-Sharp
//		Copyright (C) 2048 by N.Tsuda
//      C#への移植 by Gnico
//		License: CDDL 1.0 (http://opensource.org/licenses/CDDL-1.0)
//----------------------------------------------------------------------
using System;
using System.Windows.Forms;
namespace _2048 {
	class Program {
		static int WALL = -1;      //番人
		static int BOARD_WD = 4;   //ボード幅
		static int BOARD_HT = 4;   //ボード高さ
		static int CELL_WIDTH = 8; //セル表示幅
		static Random g_rnd;
		static bool Win = false;
		static int g_score = 0;
		static ulong g_move_count = 0;
		static int[,] g_board = new int[BOARD_WD + 2, BOARD_HT + 2];

		static void init_board() {
			g_score = 0;
			g_move_count = 0;
			Console.Clear();
			Win = false;
			for (int x = 0; x < BOARD_WD + 2; ++x)
				for (int y = 0; y < BOARD_HT + 2; ++y)
					g_board[x, y] = (x == 0 || x == BOARD_WD + 1 || y == 0 || y == BOARD_HT + 1) ?
						WALL : 0;
		}
		static void setFgBgColor(int v) {
			Console.BackgroundColor =
				 v == 0 ? ConsoleColor.Gray :
				 v == 2 ? ConsoleColor.DarkYellow :
				 v == 4 ? ConsoleColor.DarkCyan :
				 v == 8 ? ConsoleColor.DarkGreen :
				 v == 16 ? ConsoleColor.DarkBlue :
				 v == 32 ? ConsoleColor.DarkRed :
				 v == 64 ? ConsoleColor.DarkMagenta :
				 v == 128 ? ConsoleColor.Blue :
				 v == 256 ? ConsoleColor.Magenta :
				 v == 512 ? ConsoleColor.Cyan :
				 v == 1024 ? ConsoleColor.Yellow :
				 v == 2048 ? ConsoleColor.Green :
				 ConsoleColor.Black;
			Console.ForegroundColor = v <= 256 ? ConsoleColor.White :
				v > 2048 ? ConsoleColor.Black : ConsoleColor.Red;
		}
		static void print_line(int y, bool printVal) {
				for (int x = 1; x <= BOARD_WD; ++x) {
					int v = g_board[x,y];
					setFgBgColor(v);
					string str = new string(' ', CELL_WIDTH);
					if( printVal ) {
						str = v == 0 ? "." : v.ToString();
						str += new string(' ', (CELL_WIDTH - str.Length) / 2);		// 末尾に空白パディング
						str = new string(' ', CELL_WIDTH - str.Length) + str;       // 先頭に空白パディング
					}
					Console.Write(str);
				}
				Console.WriteLine();
		}
		static void print_board() {
			Console.SetCursorPosition(0, 0);
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("Score:{0}\t{1} Moved", g_score.ToString(), g_move_count.ToString());
			for (int y = 1; y <= BOARD_HT; ++y) {
				print_line(y, false);
				print_line(y, false);
				print_line(y, true);
				print_line(y, false);
				print_line(y, false);
			}
			Console.WriteLine();
		}
		static int nBlank() {
			int nBlank = 0;
			for (int y = 1; y <= BOARD_HT; ++y) 
				for (int x = 1; x <= BOARD_WD; ++x) 
					if (g_board[x,y] == 0) ++nBlank;
			return nBlank;
		}
		//	空き箇所のどこかに 2(75%) または 4(25%) を配置する
		static void put_number() {
			int n = nBlank();
			if (n == 0) return;
			g_rnd = new Random();
			int pos = g_rnd.Next(n);
			g_rnd = new Random();
			int v = g_rnd.Next(100) < 75 ? 2 : 4;		//	75% の確率で２，25%の確率で４
			//cout << "pos = " << pos << "\n";
			for (int y = 1; y <= BOARD_HT; ++y)
				for (int x = 1; x <= BOARD_WD; ++x)
					if (g_board[x, y] == 0) {		//	空欄を探す
						if (pos == 0) {
							g_board[x, y] = v;
							return;
						}
						--pos;
					}
		}
		//	盤面左右反転
		static void hrev_board() {
			for (int y = 1; y <= BOARD_HT; ++y) {
				for (int x = 1; x <= BOARD_WD / 2; ++x) {
					int cache = g_board[x, y];
					g_board[x, y] = g_board[BOARD_WD + 1 - x, y];
					g_board[BOARD_WD + 1 - x, y] = cache;
				}
			}
		}
		//	x, y 反転
		static void swapxy_board() {
			for (int y = 1; y < BOARD_HT; ++y) {
				for (int x = y + 1; x <= BOARD_WD; ++x) {
					int cache = g_board[x, y];
					g_board[x, y] = g_board[y,x];
					g_board[y,x] = cache;

				}
			}
		}
		static bool move_right() {
			bool moved = false;
			for (int y = 1; y <= BOARD_HT; ++y) {
				int dst = BOARD_WD;
				for (int src = BOARD_WD; src >= 1; --src) {
					int v = g_board[src,y];
					if (v != 0) {
						if (src == BOARD_WD) continue;		//	右端の場合
						if (g_board[dst,y] == 0) {		//	移動先が空欄
							if (dst != src) {
								g_board[src,y] = 0;
								g_board[dst,y] = v;
								moved = true;
							}
						} else if (g_board[dst,y] == v) {		//	同じ数字
							g_score += v * 2;
							g_board[src,y] = 0;
							g_board[dst,y] += v;
							--dst;
							moved = true;
						} else {
							--dst;
							if (dst != src) {
								g_board[src,y] = 0;
								g_board[dst,y] = v;
								moved = true;
							}
						}
					}
				}
			}
			return moved;
		}
		static bool move_left() {
			hrev_board();
			bool rc = move_right();
			hrev_board();
			return rc;
		}
		static bool move_up() {
			swapxy_board();
			hrev_board();
			bool rc = move_right();
			hrev_board();
			swapxy_board();
			return rc;
		}
		static bool move_down() {
			swapxy_board();
			bool rc = move_right();
			swapxy_board();
			return rc;
		}

		static bool CanMove() {
			for (int x = 1; x < BOARD_WD + 1; x++)
				for (int y = 1; y < BOARD_HT + 1; y++)
					if (g_board[x, y] == 0)
						return true;
					else if (g_board[x, y] == 2048)
						Win = true;
			for (int x = 1; x < BOARD_WD + 1; x++)
				for (int y = 1; y < BOARD_HT + 1; y++) {
					if (x - 1 > 0)
						if (g_board[x - 1, y] == g_board[x, y])
							return true;
					if (x != BOARD_HT)
						if (g_board[x + 1, y] == g_board[x, y])
							return true;
					if (y - 1 > 0)
						if (g_board[x, y - 1] == g_board[x, y])
							return true;
					if (y != BOARD_WD)
						if (g_board[x, y + 1] == g_board[x, y])
							return true;
				}
			return false;
		}

		static void Main(string[] args) {
			start:
			init_board();
			put_number();
			for (bool moved = true; ; ) {
				if (moved)
					put_number();
				print_board();
				Console.ForegroundColor = ConsoleColor.White;
				Console.BackgroundColor = ConsoleColor.Black;
				Console.WriteLine("Type Keyboad");
				Console.WriteLine("Move : ↑↓→←    Quit : \'q\'    Restart : \'r\'");
wait_key:
				ConsoleKeyInfo c = Console.ReadKey(false);
				if (c.KeyChar == 'q') Environment.Exit(0);
				if (c.KeyChar == 'r') goto start;
				switch (c.Key) {
					case ConsoleKey.LeftArrow:
						moved = move_left();
						break;
					case ConsoleKey.RightArrow:
						moved = move_right();
						break;
					case ConsoleKey.UpArrow:
						moved = move_up();
						break;
					case ConsoleKey.DownArrow:
						moved = move_down();
						break;
					default:
						goto wait_key;
				}
				if (!moved) {
					if (!CanMove())
						if (MessageBox.Show("You cannot move blocks.\nDo you want to start a new game?", "LOSE", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
							goto start;
						else
							Environment.Exit(0);
				} else {
					++g_move_count;
				}
				if (Win)
					MessageBox.Show("Win!", "Congratulations!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}
	}
}