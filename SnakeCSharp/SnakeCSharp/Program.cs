﻿//----------------------------------------------------------------------
//			snake ver 0.001
//			Copyright (C) 2014 by N.Tsuda
//          Gnico
//			Description: 
//			License: CDDL 1.0 (http://opensource.org/licenses/CDDL-1.0)
//----------------------------------------------------------------------

using System.Runtime.InteropServices;
using System.Collections.Generic;
using System;
using System.Media;
using System.IO;

namespace SnakeCSharp {
    class Program {
        static readonly int CONS_WD = 80;
        static readonly int CONS_HT = 25;
        static readonly int POINT_FOOD = 100; //餌を食べたポイント
        static readonly int EXT_FOOD = 5;     //えさを食べると伸びる胴体長
        static readonly int POINT_TIME = 10;  //一定時間毎のポイント
        static readonly int TERM = 80;        //一定時間
        static readonly int N_FOOD = 10;      //初期餌数
        static readonly int SP_N_FOOD = 1; //スペシャル餌の初期数
        static readonly int SP_ADD_TICKS = 30; //スペシャル餌の追加頻度
        static readonly int SP_POINT_FOOD = 500; //スペシャル餌を食べたポイント
        static readonly int BROWN_ADD_TICKS = 120; //茶色い物体の追加頻度
        static Random rand;

        /// <summary>
        /// 効果音の再生
        /// </summary>
        /// <param name="ums">再生するストリーム</param>
        /// <param name="sp">サウンドプレイヤー</param>
        static void PlaySound(UnmanagedMemoryStream ums,ref SoundPlayer sp) {
            if (sp != null) {
                sp.Stop();
                sp.Dispose();
                sp = null;
            }
            sp = new System.Media.SoundPlayer(ums);
            sp.Play();
        }

        /// <summary>
        /// 非同期でキーが押されているかを取得します
        /// </summary>
        /// <param name="vKey">判定するキー</param>
        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(int vKey);
        /// <summary>
        /// 非同期でキーが押されているかを取得します
        /// </summary>
        /// <param name="vKey">判定するキー</param>
        /// <returns>押されているか否か</returns>
        static bool isKeyPressed( int vKey ) { return (GetAsyncKeyState(vKey) & 0x8000) != 0; }

        /// <summary>
        ///	乱数で食料位置を生成し、foods に追加する
        ///	重なりチェックは行わない        
        /// </summary>
        /// <param name="foods">食べ物情報のリスト</param>
        /// <param name="cnt">生成数</param>
        static void add_foods( ref List<Tuple<int, int>> foods, int cnt ) {
            rand= new Random();
            for( int i = 0; i < cnt; i++ ) {
                int x = rand.Next(CONS_WD - 2) + 1;
                int y = rand.Next(CONS_HT - 4) + 2;
                foods.Add(new Tuple<int,int>(x, y));
            }
        }

        /// <summary>
        ///	餌を画面に表示 
        /// </summary>
        /// <param name="foods">食物情報のリスト</param>
        /// <param name="SP">スペシャルフードか否か</param>
        static void print_foods( List<Tuple<int, int>> foods, bool SP ) {
            Console.ForegroundColor = SP ? ConsoleColor.Cyan : ConsoleColor.Green;
            foods.ForEach(t => {
                Console.SetCursorPosition(t.Item1, t.Item2);
                Console.Write(SP ? "#" : "$");
            });
        }

        /// <summary>
        /// 餌を食べたかチェックをします
        /// </summary>
        /// <param name="foods">食べ物リスト</param>
        /// <param name="x">虫の頭のx軸</param>
        /// <param name="y">虫の頭のy軸</param>
        /// <returns>食べているかチェック</returns>
        static bool check_foods( List<Tuple<int, int>> foods, int x, int y ) {
            int i = foods.IndexOf(new Tuple<int, int>(x, y));
            if( i != -1 ) {
                foods.RemoveAt(i);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 版を表示します(囲いや、スコアなどを表示するための一行など…)
        /// </summary>
        static void print_field() {
            Console.Clear();
            Console.BackgroundColor = Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, 1);
            for( int i = 0; i < CONS_WD; ++i ) 
                Console.Write(" ");
            for( int y = 2; y < CONS_HT - 2; ++y ) {
                Console.SetCursorPosition(0, y);
                Console.Write(" ");
                Console.SetCursorPosition(CONS_WD - 1, y);
                Console.Write(" ");
            }
            Console.SetCursorPosition(0, CONS_HT - 2); //最下行の一つ上
            Console.Write(new string(' ', CONS_WD));
            Console.BackgroundColor = ConsoleColor.Black; //黒背景
            for( int y = 0; y < CONS_HT - 4; ++y ) {
                Console.SetCursorPosition(1, 2);
                Console.Write(new string(' ', CONS_WD - 2));
            }
            Console.SetCursorPosition(0, CONS_HT - 1); //最下行
            Console.Write( new string(' ', CONS_WD - 1));
        }

        /// <summary>
        /// スコアなどの表示
        /// </summary>
        /// <param name="score">スコア</param>
        /// <param name="bodyLength">虫の体の長さ</param>
        /// <param name="PressedKey">押しているキー</param>
        static void print_score( int score, int bodyLength, byte PressedKey ) {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, 0);
            Console.Write("SCORE:");
            Console.Write(score);
            Console.Write("\tBODY:");
            Console.Write(bodyLength);
            Console.Write("\tPressedKey:");
            switch( PressedKey ) {
                case 0: Console.Write("NONE "); break;
                case 1: Console.Write("LEFT "); break;
                case 2: Console.Write("DOWN "); break;
                case 3: Console.Write("UP   "); break;
                case 4: Console.Write("RIGHT"); break;
                case 5: Console.Write("SPACE"); break;
            }
        }

        /// <summary>
        /// 虫の頭、胴体部分を表示します
        /// </summary>
        /// <param name="snake">虫情報のリスト</param>
        static void print_snake( ref List<Tuple<int, int>> snake ) {
            //胴体部分を描画
            for( int i = 1; i < snake.Count; ++i ) {
                Console.ForegroundColor = i % 2 == 1 ? ConsoleColor.Blue : ConsoleColor.Red;
                Console.SetCursorPosition(snake[i].Item1, snake[i].Item2);
                Console.Write("*");
            }
            //重なった場合でも、腰部を表示するため、最後に頭部を描画
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.SetCursorPosition(snake[0].Item1, snake[0].Item2);
            Console.Write("@"); //頭部
        }

        /// <summary>
        /// 虫の位置情報・長さを更新します
        /// </summary>
        /// <param name="snake">虫のリスト</param>
        /// <param name="x">虫の行くx軸</param>
        /// <param name="y">虫の行くy軸</param>
        /// <param name="extend">伸びるか</param>
        static void update_snake( ref List<Tuple<int, int>> snake, int x, int y, bool extend ) {
            snake.Insert(0, new Tuple<int, int>(x, y));
            if( !extend ) { //胴体が伸びない場合、末尾に空白を表示、胴体を消す
                Console.SetCursorPosition(snake[snake.Count - 1].Item1, snake[snake.Count - 1].Item2);
                Console.Write(" ");
                snake.RemoveAt(snake.Count - 1);
            }
        }

        /// <summary>
        /// 壁または、虫の胴体とぶつかったかチェックします
        /// </summary>
        /// <param name="snake">虫のリスト</param>
        /// <returns>ぶつかったか否か</returns>
        static bool collapsed( ref List<Tuple<int, int>> snake ) {
            int x = snake[0].Item1;
            int y = snake[0].Item2;
            if( x <= 0 || x >= CONS_WD - 1 || y <= 1 || y >= CONS_HT - 2 )
                return true; //周辺の壁に激突！
            for( int i = 1; i < snake.Count; ++i ) 
                if( snake[i].Item1 == x && snake[i].Item2 == y ) 
                    return true; //自分自身に激突！あいたたた・・・
            return false;
        }

        /// <summary>
        /// 胴体方向かどうかチェックします
        /// </summary>
        /// <param name="snake"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <returns>胴体がなければtrueします</returns>
        static bool check_body( ref List<Tuple<int, int>> snake, int dx, int dy ) {
            if( snake.Count < 2 )
                return true; // 胴体がない場合
            return snake[0].Item1 + dx != snake[1].Item1 || snake[0].Item2 + dy != snake[1].Item2;
        }

        //茶、茶色い物体を・・・おぅ・・
        static void brown_add( List<Tuple<int, int>> snake, ref List<Tuple<int, int>> brown ) {
            if( snake[snake.Count - 2].Item1 < snake[snake.Count - 1].Item1 )
                if( snake[snake.Count - 1].Item1 < CONS_WD )
                    brown.Add(new Tuple<int, int>(snake[snake.Count - 1].Item1 + 1, snake[snake.Count - 1].Item2));
            else if( snake[snake.Count - 2].Item1 > snake[snake.Count - 1].Item1 )
                brown.Add(new Tuple<int, int>(snake[snake.Count - 1].Item1 - 1, snake[snake.Count - 1].Item2));
            else if( snake[snake.Count - 2].Item2 < snake[snake.Count - 1].Item2 )
                if( snake[snake.Count - 1].Item1 < CONS_HT )
                    brown.Add(new Tuple<int, int>(snake[snake.Count - 1].Item1, snake[snake.Count - 1].Item2 + 1));
            else if( snake[snake.Count - 2].Item2 < snake[snake.Count - 1].Item2 )
                brown.Add(new Tuple<int, int>(snake[snake.Count - 1].Item1, snake[snake.Count - 1].Item2 - 1));
        }
        //茶色い物体の表示
        static void brown_print( List<Tuple<int, int>> brown ) {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            foreach( Tuple<int,int> t in brown ) {
                Console.SetCursorPosition(t.Item1, t.Item2);
                Console.Write("&");
            }
        }

        static void Main( string[] args ) {
            Console.Title = "ヘビさんゲーム for C♯";
            SoundPlayer sp = null;
            while( true ) {
                int Ticks = 2;
                int score = 0; //スコアの初期化
                int x = CONS_WD / 2; //初期位置座標
                int y = CONS_HT / 2;
                int dx = 0, dy = 1; //速度ベクター
                int eating = 0; //餌食べフラグ
                byte PressedKey = 0;
                List<Tuple<int, int>> snake = new List<Tuple<int, int>>() { new Tuple<int, int>(x, y - 2), new Tuple<int, int>(x, y - 1), new Tuple<int, int>(x, y) };
                List<Tuple<int,int>> foods = new List<Tuple<int, int>>(); //スネーク座標情報、frontが頭・食べ物位置リスト
                List<Tuple<int, int>> brown_obj = new List<Tuple<int, int>>();
                List<Tuple<int, int>> Specialfoods = new List<Tuple<int, int>>();
                add_foods(ref foods, N_FOOD); //食べ物位置リストに食べ物を追加
                add_foods(ref Specialfoods, SP_N_FOOD);
                bool extend = false;
                Console.Clear();
                print_score(score, snake.Count, PressedKey);
                Console.SetCursorPosition(20, 11);
                Console.Write("何かキーを押すとスキップ");
                Console.SetCursorPosition(20, 10);
                Console.Write("ゲームスタートまで:");
                Console.BackgroundColor = ConsoleColor.Red;
                Console.SetCursorPosition(20, 9);
                Console.Write("        Ready?        ");
                Console.BackgroundColor = ConsoleColor.Black;
                for( byte b = 0; b < 31; b++ ) {
                    if( Console.KeyAvailable )
                        break;
                    Console.SetCursorPosition(39, 10);
                    Console.Write((30 - b).ToString() + " ");
                    System.Threading.Thread.Sleep(100);
                }
                print_field();
                print_foods(foods, false);
                print_foods(Specialfoods, true);
                print_snake(ref snake);
                    while( true ) {
                        if( isKeyPressed(0x20) ) {
                            Console.Clear();
                            print_score(score, snake.Count, 5);
                            Console.SetCursorPosition(0, 10);
                            Console.Write("PAUSE");
                            System.Threading.Thread.Sleep(1000);
                            while( true ) {
                                if( isKeyPressed(0x20) ) {
                                    print_field();
                                    print_foods(foods, false);
                                    print_foods(Specialfoods, true);
                                    print_snake(ref snake);
                                    for( int i = 0; i <= 5; i++ ) {
                                        Console.SetCursorPosition(0, 0);
                                        Console.Write("Wait {0} miliseconds", (5 - i).ToString() + "00");
                                        System.Threading.Thread.Sleep(100);
                                    }
                                    break;
                                }
                                System.Threading.Thread.Sleep(50);
                            }
                        }
                        //キー入力による方向転換
                        if( (isKeyPressed('H') || isKeyPressed(0x25)) && check_body(ref snake, -1, 0) ) {
                            dx = -1;
                            dy = 0;
                            PressedKey = 1;
                        } else if( (isKeyPressed('J')) || isKeyPressed(0x28) && check_body(ref snake, 0, 1) ) {
                            dx = 0;
                            dy = 1;
                            PressedKey = 2;
                        } else if( (isKeyPressed('K') || isKeyPressed(0x26)) && check_body(ref snake, 0, -1) ) {
                            dx = 0;
                            dy = -1;
                            PressedKey = 3;
                        } else if( (isKeyPressed('L') || isKeyPressed(0x27)) && check_body(ref snake, 1, 0) ) {
                            dx = 1;
                            dy = 0;
                            PressedKey = 4;
                        }
                        extend = Ticks >= TERM && Ticks % TERM == 0;
                        if( Ticks % 4 == 0 ) {
                            x += dx;
                            y += dy;
                            if( eating % 2 == 1 || eating % 2 == -1 ) {
                                --eating;
                                extend = true; //餌を食べた場合は、胴体を徐々に伸ばす 
                            }
                            update_snake(ref snake, x, y, extend);
                        }
                        if( Ticks >= TERM && Ticks % TERM == 0 ) {
                            score += POINT_TIME; //一定期間米にポイント追加
                            add_foods(ref foods, 1); //餌を一個追加
                            if( Ticks % SP_ADD_TICKS == 0 )
                                add_foods(ref Specialfoods, 1);
                        }
                        if( Ticks == BROWN_ADD_TICKS ) {
                            brown_add(snake, ref brown_obj);
                            Ticks = 5;
                        }
                        if( check_foods(foods, x, y) ) {
                            PlaySound(Properties.Resources.powerUp, ref sp);
                            eating = EXT_FOOD; //餌を食べると胴体が伸びる
                            score += POINT_FOOD;
                        }
                        if( check_foods(Specialfoods, x, y) ) {
                            PlaySound(Properties.Resources.powerUp, ref sp);
                            eating = EXT_FOOD; //餌を食べると胴体が伸びる
                            score += SP_POINT_FOOD;
                        }
                        if( Ticks % 2 == 0 ) {
                            print_foods(foods, false);
                            print_foods(Specialfoods, true);
                            brown_print(brown_obj);
                            print_snake(ref snake);
                            print_score(score, snake.Count, PressedKey);
                        }
                        if( collapsed(ref snake) ) {
                            PlaySound(Properties.Resources.GameOver, ref sp);
                            break;
                        }
                        if( check_foods(brown_obj, x, y) ) {
                            PlaySound(Properties.Resources.Brown_Eat, ref sp);
                            break;
                        }
                        Ticks += 1;
                        System.Threading.Thread.Sleep(50); //50ミリ秒ウェイト
                    }
                Console.SetCursorPosition(0, CONS_HT - 1);
                Console.Write("もう一度挑戦しますか？[Y/N]");
                while (true){
                    if (isKeyPressed('Y') | isKeyPressed('y'))
                        break;
                    if(isKeyPressed('N') | isKeyPressed('n'))
                        Environment.Exit(0);
                }
                
            }
        }
    }
}
/*
ToDo:
◎ 胴体の方向には方向転換出来ない方がよい
◎ 餌を食べたら＋得点、胴体の伸ばす
◎ 胴体長を画面上部に表示
◎ 餌を一定時間毎に増やす
◎ カーソルキーでも操作可能に
*/
