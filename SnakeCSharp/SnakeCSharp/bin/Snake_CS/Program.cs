﻿//----------------------------------------------------------------------
//			snake ver 0.001
//			Copyright (C) 2014 by N.Tsuda
//          Gnico
//			Description: 
//			License: CDDL 1.0 (http://opensource.org/licenses/CDDL-1.0)
//----------------------------------------------------------------------

using System.Runtime.InteropServices;
using System.Collections.Generic;
using System;

namespace SnakeCSharp {
    class Program {
        static int CONS_WD = 80;
        static int CONS_HT = 25;
        static int POINT_FOOD = 100; //餌を食べたポイント
        static int EXT_FOOD = 5;     //えさを食べると伸びる胴体長
        static int POINT_TIME = 10;  //一定時間毎のポイント
        static int TERM = 10;        //一定時間
        static int N_FOOD = 10;      //初期餌数
        static Random rand;

        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(int vKey);
        /* NOTE
         * C# や VB や F#では、DllImport(通称：P/Invoke)を使って、WinAPIやC++の関数を呼び出します 
         */
        /* NOTE
         * C++での、Vector<>は、C#では、List<>になります。
         * C++での、pair<>は、C#では、Tuple<>になります。このTupleは現在主流の言語ではかならずあると言ってもいいほどのクラスです
         * C#では、座標を表す、Point型がありますが、動作をC++に近づけようということで、Tupleを使用します。
         * 詳しいことは、MSDNを見てください(丸投げ)
         * NOTE2
         * 背景色指定は、Console.BackgroundColor = (enum ConsoleColor)で、
         * 文字色指定は、Console.ForegroundColor = (enum ConsoleColor)で、
         * することが出来ます。
         * カーソル位置は、Console.SetCursorPosition(int x, int y)で指定できます。
         */
        static bool isKeyPressed( int vKey ) { return (GetAsyncKeyState(vKey) & 0x8000) != 0; }

        //	乱数で食料位置を生成し、foods に追加する
        //	重なりチェックは行わない
        static void add_foods( ref List<Tuple<int, int>> foods, int cnt ) {
            rand= new Random(Environment.TickCount);
            for( int i = 0; i < cnt; i++ ) {
                int x = rand.Next(CONS_WD - 2) + 1;
                int y = rand.Next(CONS_HT - 4) + 2;
                foods.Add(new Tuple<int,int>(x, y));
            }
        }

        //	餌を画面に表示
        static void print_foods( List<Tuple<int, int>> foods ) {
            Console.ForegroundColor = ConsoleColor.Green;
            for( int i = 0; i < foods.Count; ++i ) {
                Console.SetCursorPosition(foods[i].Item1, foods[i].Item2);
                Console.Write("$");
            }
        }

        //	(x, y)：スネークの頭位置
        //	return: 餌を食べた場合は true を返す
        static bool check_foods( List<Tuple<int, int>> foods, int x, int y ) {
            for( int i = 0; i < foods.Count; ++i ) {
                if( foods[i].Item1 == x && foods[i].Item2 == y ) {
                    foods.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        static void print_field() {
            Console.Clear(); //面倒くさいから、クリアするよ
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, 1);
            for( int i = 0; i < CONS_WD; ++i ) {
                Console.Write(" ");
            }
            for( int y = 2; y < CONS_HT - 2; ++y ) {
                Console.SetCursorPosition(0, y);
                Console.Write(" ");
                Console.SetCursorPosition(CONS_WD - 1, y);
                Console.Write(" ");
            }
            Console.SetCursorPosition(0, CONS_HT - 2); //最下行の一つ上
            for( int i = 0; i < CONS_WD; ++i ) {
                Console.Write(" ");
            }
            Console.BackgroundColor = ConsoleColor.Black; //黒背景
            for( int y = 0; y < CONS_HT - 4; ++y ) {
                Console.SetCursorPosition(1, 2);
                for (int x = 0; x < CONS_WD - 2; ++ x){
                    Console.Write(" ");
                }
            }
            Console.SetCursorPosition(0, CONS_HT - 1); //最下行
            for( int i = 0; i < CONS_WD - 1; ++i ) {
                Console.Write(" ");
            }
        }

        static void print_score( int score, int bodyLength ) {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, 0);
            Console.Write("SCORE:");
            Console.Write(score);
            Console.Write("\tBODY:");
            Console.Write(bodyLength);
        }

        //スネークの頭、胴体部分を表示
        static void print_snake( ref List<Tuple<int, int>> snake ) {
            //胴体部分を描画
            Console.ForegroundColor = ConsoleColor.Blue;
            for( int i = 1; i < snake.Count; ++i ) {
                Console.SetCursorPosition(snake[i].Item1, snake[i].Item2);
                Console.Write("*");
            }
            //重なった場合でも、腰部を表示するため、最後に頭部を描画
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.SetCursorPosition(snake[0].Item1, snake[0].Item2);
            Console.Write("@"); //頭部
        }

        //スネーク位置、同体長を更新
        static void update_snake( ref List<Tuple<int, int>> snake, int x, int y, bool extend ) {
            snake.Insert(0, new Tuple<int, int>(x, y));
            if( !extend ) { //胴体が伸びない場合、末尾に空白を表示、胴体を消す
                Console.SetCursorPosition(snake[snake.Count - 1].Item1, snake[snake.Count - 1].Item2);
                Console.Write(" ");
                snake.RemoveAt(snake.Count - 1);
            }
        }

        //壁又はスネークの胴体とぶつかったかどうかをチェック
        static bool collapsed( ref List<Tuple<int, int>> snake ) {
            int x = snake[0].Item1;
            int y = snake[0].Item2;
            if( x <= 0 || x >= CONS_WD - 1 || y <= 1 || y >= CONS_HT - 2 )
                return true; //周辺の壁に激突！
            for( int i = 1; i < snake.Count; ++i ) {
                if( snake[i].Item1 == x && snake[i].Item2 == y ) 
                    return true; //自分自身に激突！あいたたた・・・
            }
            return false;
        }

        //胴体方向かどうかチェック
        //return:胴体がなければtrueを返す
        static bool check_body( ref List<Tuple<int, int>> snake, int dx, int dy ) {
            if( snake.Count < 2 )
                return true; // 胴体がない場合
            return snake[0].Item1 + dx != snake[1].Item1 || snake[0].Item2 + dy != snake[1].Item2;
        }

        static void Main( string[] args ) {
            while( true ) {
                int score = 0; //スコアの初期化
                int x = CONS_WD / 2; //初期位置座標
                int y = CONS_HT / 2;
                int dx = 0, dy = 1; //速度ベクター
                List<Tuple<int, int>> snake = new List<Tuple<int,int>>(), foods = new List<Tuple<int,int>>(); //スネーク座標情報、frontが頭・食べ物位置リスト
                snake.Add(new Tuple<int,int>(x, y - 2));
                snake.Add(new Tuple<int,int>(x, y - 1));
                snake.Add(new Tuple<int,int>(x, y));
                add_foods(ref foods, N_FOOD); //食べ物位置リストに食べ物を追加
                print_field();
                print_foods(foods);
                print_snake(ref snake);
                print_score(score, snake.Count);
                int eating = 0; //餌食べフラグ
                for (int cnt = 0; ; ++cnt){
                    //キー入力による方向転換
                    if ( (isKeyPressed('H') || isKeyPressed(0x25) ) && check_body(ref snake,-1,0)){
                        dx = -1;
                        dy = 0;
                    }else if ( (isKeyPressed('J')) || isKeyPressed(0x28) && check_body(ref snake,0,1)){
                        dx = 0;
                        dy = 1;
                    }else if((isKeyPressed('K') || isKeyPressed(0x26)) && check_body(ref snake,0,-1)){
                        dx = 0;
                        dy = -1;
                    }else if( (isKeyPressed('L') || isKeyPressed(0x27)) && check_body(ref snake,1,0)){
                        dx = 1;
                        dy = 0;
                    }
                    x += dx;
                    y += dy;
                    bool extend = cnt % TERM == TERM - 1; //TERM回に1回動体を伸ばす
                    if( extend ) {
                        score += POINT_TIME; //一定期間米にポイント追加
                        add_foods(ref foods, 1); //餌を一個追加
                    }
                    if (eating % 2 == 1　|| eating % 2 == -1){
                        --eating;
                        extend = true; //餌を食べた場合は、胴体を徐々に伸ばす
                    }
                    update_snake(ref snake,x,y,extend);
                    if (check_foods(foods,x , y )){
                        Console.Beep(); //ビープ
                        eating = EXT_FOOD; //餌を食べると胴体が伸びる
                        score += POINT_FOOD;
                    }
                    print_foods(foods);
                    print_snake(ref snake);
                    print_score(score,snake.Count);
                    if (collapsed(ref snake)){
                        Console.Beep();
                        Console.Beep();
                        Console.Beep(); //ビープ三回！うるさいね！
                        break;
                    }
                    System.Threading.Thread.Sleep(200); //200ミリ秒ウェイト
                }
                Console.SetCursorPosition(0, CONS_HT - 1);
                Console.Write("try again?");
                while (true){
                    if (isKeyPressed('Y'))
                        break;
                    if(isKeyPressed('N'))
                        Environment.Exit(0);
                }
            }
        }
    }
}
/*
ToDo:
◎ 胴体の方向には方向転換出来ない方がよい
◎ 餌を食べたら＋得点、胴体の伸ばす
◎ 胴体長を画面上部に表示
◎ 餌を一定時間毎に増やす
◎ カーソルキーでも操作可能に
*/
